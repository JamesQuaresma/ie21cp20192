# Abajur por sensor de proxímidade !

## Descrição
Abajur desenvolvido para ser ligado e desligado com a ausência e com a 
presença humana no ambiente, atravês de um sensor de proximidade, 
enquanto estiver pessoas no ambiente ele ligará e com ausência de presença
humana se desliga.

Web Page https://jamesquaresma.github.io/

## Lista de material utilizado para conclusão do projeto
* 1-Kit arduino uno
* 1-Sensor de proxímidade ultrasônico modelo : hc-sr04
* Resistores
* Jumpers
* 1-lampada 127 Vts
* 1-Bocal
* 2-Metros de fio
* 1-Relê 5 woltts
* 1-Led pequena azul
* 1-Led pequena vermelha