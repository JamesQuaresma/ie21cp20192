---
title: "Introduction"
date: 2019-10-10T16:24:51-03:00
draft: true
---


Um abajur desenvolvido para que eu possa trabalhar com uma luminosidade baixa.
A partir do momento que eu me ausento do quarto ou da minha mesa de projeto ele se desliga;
e enquanto eu estiver proxímo ou a uma distancia de dois metros ele se matem ligado.
Uma led vermelha foi instalada para sinalizar que estou fora dos dois metros de distância do projeto.
Uma led azul irá ligar quando estiver a uma distância de dois metros do projeto ligando o abajur
