# Introdução
Um abajur desenvolvido para que eu possa trabalhar com uma luminosidade baixa.
A partir do momento que eu me ausento do quarto ou da minha mesa de projeto ele se desliga;
e enquanto eu estiver proxímo ou a uma distancia de dois metros ele se matem ligado.

## Equipe

O projeto foi desenvolvido pelo aluno de Engenharia de Computação:

|Nome|Usuário|
|---|---|
|Lucas de O. Santos|JamesQuaresma|
|-|-|
|-|-|

# Documentação
A documentação do projeto pode ser acessada pelo link:

> https://jamesquaresma.gitlab.io/ie21cp20192 ;
Confira o video do projeto no youtube no link :
https://www.youtube.com/watch?v=Gpl0feh3Uo0


*1-Kit arduino uno;

*1-Sensor de proxímidade ultrasônico modelo : hc-sr04;

**Resistores;

**Jumpers;

*1-lampada 127 Vts;

*1-Bocal

*2-Metros de fio;

*1-Modulo Relê 5 woltts;

*1-Led pequena azul;

*1-Led pequena vermelha;

# Links Úteis

* [Kit de Desenvolvimento Arduino](https://www.arduino.cc/)
* [Sintax Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [Hugo Website](https://gohugo.io/)