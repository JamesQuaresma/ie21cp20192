/*
Projeto Arduino
Arduino com sensor de proximidade ultrasonico HHC-SRO4
*/
//Incluindo biblioteca Ultrasonic.h
#include "Ultrasonic.h"
 
//criando objeto ultrasonic e definindo as portas digitais 
//do Trigger - 5 - e Echo - 4
Ultrasonic ultrasonic(5,4);
 
//Declaração das constantes referentes aos pinos digitais.
const int led_1 = 9;
const int led_2 = 8;
 
long microsec = 0;
float distanciaCM = 0;

void ledDistancia() {
   
  //Acendendo o led adequado para a distância lida no sensor
  if (distanciaCM >= 150) {
    digitalWrite(led_1,HIGH);
    digitalWrite(led_2,LOW);
    
  }
   
  else {
    digitalWrite(led_2,HIGH);
    digitalWrite(led_1,LOW);
  }
}
 
void setup() {
  Serial.begin(115200); //Inicializando o serial monitor
   
  //Definindo pinos digitais
  pinMode(led_1,OUTPUT); //9 como de saída.
  pinMode(led_2,OUTPUT); //8 como de saída.
}
 
void loop() {  
  //Lendo o sensor
  microsec = ultrasonic.timing(); 
 
  //Convertendo a distância em CM
  distanciaCM = ultrasonic.convert(microsec, Ultrasonic::CM); 
 
  ledDistancia();

  Serial.print(distanciaCM);
  Serial.println(" cm");
    distanciaCM = 0;
  delay(1000);
}
//Método que centraliza o controle de acendimento dos leds.